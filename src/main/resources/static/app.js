var socket = null;
function connect(){
	console.log("connecting");
	socket = new WebSocket('ws://' + window.location.host + '/my-websocket-endpoint');
	socket.onopen = function() {
		  console.log('WebSocket connection opened. Ready to send messages.');
		  setConnected(true);
	};
		
	socket.onmessage = function(message) {
		  console.log('Message received from server: ' + message);
		  receive(message);
	};
	
    ws.onclose = function() { 
        console.log("Connection is closed..."); 
    };
}

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function disconnect() {
	console.log("disconnecting");
	socket.close();
}

function send() {
	console.log("sending: " + $("#name").val());
    socket.send($("#name").val());
}

function receive(message) {
	$("#greetings").append("<tr><td>" + message.data + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { send(); });
});